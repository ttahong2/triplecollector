package ex;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.*;

public class TestMain {
	@Test
	public void query_test() {
		Sparql s = new Sparql();
		String result;
		String query1 = "select distinct ?URI where {?URI foaf:name \"Hans Lunding\"@en;     foaf:name ?name.    FILTER  langMatches( lang(?name), \"*\" )} LIMIT 10";
		String query2 = "select distinct ?URI where {?URI foaf:name ?name.    FILTER (langMatches( lang(?name), \"*\" )&& regex(?name,\"hans lunding\",\"i\"))} LIMIT 10";
		String query3 = "select distinct ?URI where{?URI foaf:name ?name. FILTER  (?name=\"Hans Lunding\"@en && langMatches( lang(?name), \"*\" )) } LIMIT 10";

		result = s.urlCall(query3, CONST.ENDPOINT, CONST.FORMATPARAM_JSON);

		System.out.println(result);

	}

	@Ignore
	public void json_test() {
		Sparql s = new Sparql();
		String result;
		String query1 = "select distinct ?URI where {?URI foaf:name \"Hans Lunding\"@en;     foaf:name ?name.    FILTER  langMatches( lang(?name), \"*\" )} LIMIT 10";
		String query2 = "select distinct ?URI where {?URI foaf:name ?name.    FILTER (langMatches( lang(?name), \"*\" )&& regex(?name,\"hans lunding\",\"i\"))} LIMIT 10";
		String query3 = "select distinct ?URI where{?URI foaf:name ?name. FILTER  (?name=\"Hans Lunding\"@en && langMatches( lang(?name), \"*\" )) } LIMIT 10";

		result = s.urlCall(query3, CONST.ENDPOINT, CONST.FORMATPARAM_JSON);

		Parser p = new Parser();
		p.JsonParse(result);

	}

}
