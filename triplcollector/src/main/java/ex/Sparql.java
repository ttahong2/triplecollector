package ex;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Iterator;



public class Sparql {
	
	public Contents getURI(Contents c){
		String term= c.getTerm();
		//String query1 = "select distinct ?URI where {?URI foaf:name \""+ term+"\"@en;     foaf:name ?name.    FILTER  langMatches( lang(?name), \"*\" )} LIMIT 10";
		String query2 = "select distinct ?URI where {?URI foaf:name ?name.    FILTER (langMatches( lang(?name), \"*\" )&& regex(?name,\""+term+"\",\"i\"))} LIMIT 10";
		//String query3 = "select distinct ?URI where{?URI foaf:name ?name. FILTER  (?name=\""+term+"\"@en && langMatches( lang(?name), \"*\" )) } LIMIT 10";
		Parser p = new Parser();
		p.parseURI(c,urlCall(query2,CONST.ENDPOINT,CONST.FORMATPARAM_JSON));
		return c;
	}
	
	public Contents getContents(Contents c){
		Iterator<ContentsItem> iter= c.getContent().iterator(); 
		ContentsItem tempItem;
		String URI="";
		String query;
		while(iter.hasNext())
		{
			tempItem=iter.next();
			URI=tempItem.getLink();
			query="Describe <"+URI+">";
			tempItem.setContent(urlCall(query,CONST.ENDPOINT,CONST.CONTENT_FORMAT));
		}
		return c;
	}
	
	
	
	//private Logger log = LoggerFactory.getLogger(this.getClass());
	public String urlCall(String query, String EndPoint, String Format){
		String result="";
		try{
			String encodedQuery = URLEncoder.encode(query, "UTF-8");
			
			URL url = new URL(EndPoint);
			URLConnection urlConnection = url.openConnection();

			// Set the desired param
			urlConnection.setRequestProperty("Accept", Format);
			urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Linux; X11)");
			urlConnection.setDoOutput(true);
			urlConnection.setAllowUserInteraction(false);
			urlConnection.setConnectTimeout(9999);

			PrintStream serviceCallStream = new PrintStream(urlConnection
					.getOutputStream());
			serviceCallStream.println("query=" + encodedQuery);
			serviceCallStream.close();
			
			BufferedReader serviceResponseStream = new BufferedReader(
					new InputStreamReader(urlConnection.getInputStream()));
			
			// retrieve result
			int readChars = 0;
			StringBuilder responseBuilder = new StringBuilder();
			char[] buffer = new char[1024 * 1024];
			while ((readChars = serviceResponseStream.read(buffer)) > 0) {
				responseBuilder.append(buffer, 0, readChars);
			}
			serviceResponseStream.close();
			result = responseBuilder.toString();
			//result null 인경우
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return result;
	}
}
