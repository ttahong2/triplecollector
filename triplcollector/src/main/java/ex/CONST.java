package ex;

public class CONST {
	public static final String ENDPOINT="http://dbpedia.org/sparql";
	public static final String FORMATPARAM_N3="text/n3";
	public static final String FORMATPARAM_RDF="application/rdf+xml";
	public static final String FORMATPARAM_JSON="application/json";
	public static final String PREFIX="PREFIX d2r: <http://sites.wiwiss.fu-berlin.de/suhl/bizer/d2r-server/config.rdf#>"
			+"PREFIX swrc: <http://swrc.ontoware.org/ontology#>" +
			"PREFIX dcterms: <http://purl.org/dc/terms/>"+
			"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"+
			"PREFIX dc: <http://purl.org/dc/elements/1.1/>"+
			"PREFIX map: <file:///home/diederich/d2r-server-0.3.2/dblp-mapping.n3#>"+
			"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>" +
			"PREFIX foaf: <http://xmlns.com/foaf/0.1/>"+
			"PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"+
			"PREFIX owl: <http://www.w3.org/2002/07/owl#> ";
	public static final String CONTENT_FORMAT = FORMATPARAM_N3;
}
