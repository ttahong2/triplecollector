/*ContentsItem.java
*Created on 2012. 9. 20.
*/
package ex;

/**
 * Class Summary & Description
 *
 *
 *
 * @author 김태홍
 * @version 1.0, 2012. 9. 20.
 */
public class ContentsItem {
	String link=null;	
	String explicit_content=null;
	
	public ContentsItem(String Urilink){
		this.link=Urilink;		
	}	
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getContent() {
		return explicit_content;
	}
	public void setContent(String explicit_content) {
		this.explicit_content = explicit_content;
	}
	@Override
	public String toString() {
		String print[] = explicit_content.split("\n");
		return print.toString();
	}
}
