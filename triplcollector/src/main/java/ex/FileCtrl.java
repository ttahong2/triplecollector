package ex;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.util.Iterator;

import javax.swing.JTextArea;


public class FileCtrl {
	public void readFile(String inputDirName) {
		String currentDir = System.getProperty("user.dir");
		String SubDir = currentDir + "\\" + inputDirName + "\\";
		File in = new File(SubDir);
		if (!in.exists()) {
			System.out.println("입력파일이 없습니다.");
		} else {
			String list[] = in.list();
			String Keyword;
			for (int i = 0; i < list.length; i++) {
				try {
					FileReader fr = new FileReader(SubDir + list[i]);
					BufferedReader read = new BufferedReader(fr);
					while ((Keyword = read.readLine()) != null) {
						//run();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}

	}
	public void saveResult(Contents c){
		Iterator<ContentsItem> iter= c.getContent().iterator(); ; 
		ContentsItem tempItem;
		int n=0;
		while(iter.hasNext())
		{
			tempItem=iter.next();
			String Con= "#"+tempItem.getLink()+"\r\n"+tempItem.getContent();
			saveFileUTF(c.getTerm()+"_"+n+".nt",Con);
			n++;
		}
	}
	
	private void saveFileUTF(String name, String cont){
		String currentDir= System.getProperty("user.dir");
		String SubDir= currentDir+"\\output\\";
		String FileName= SubDir+name;

		File f = new File(FileName);
		if(f.exists()){
			f.delete();
		}

		File d= new File(SubDir);
		if(!d.exists()){
			d.mkdirs();
			System.out.println(SubDir+" 디렉토리 생성 완료");
		}    

		try{
			OutputStreamWriter writer = new OutputStreamWriter(
					new FileOutputStream(FileName, true), "UTF-8");
			BufferedWriter bw= new BufferedWriter(writer);
			bw.write(cont);
			bw.close();
			writer.close();
		}catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
		System.out.println("File writing completed: "+FileName);
	}
	
}
