package ex;

import java.util.ArrayList;

public class Contents {

	private String term="";
	private ArrayList<ContentsItem> content = new ArrayList<ContentsItem>();
	
	public ArrayList<ContentsItem> getContent() {
		return content;
	}
	public void setContent(ArrayList<ContentsItem> content) {
		this.content = content;
	}
	public String getTerm() {
		return term;
	}
	
	public Contents(String word){
		this.term=word;
	}
}
