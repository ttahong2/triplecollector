package ex;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;


public class Start {
	//입력 파일 디렉토리 : ./input
	//출력 파일 디렉토리 : ./output
	
	
	
	
	public static void main(String[] args) {
		Start s= new Start();
		String inputDir="input";
		s.start(inputDir);	
		System.out.println("완료");
	}
	
	private void start(String inputDirName) {
		String currentDir = System.getProperty("user.dir");
		String SubDir = currentDir + "\\" + inputDirName + "\\";
		File in = new File(SubDir);
		if (!in.exists()) {
			System.out.println("입력파일이 없습니다.");
		} else {
			String list[] = in.list();
			String Keyword;
			for (int i = 0; i < list.length; i++) {
				try {
					FileReader fr = new FileReader(SubDir + list[i]);
					BufferedReader read = new BufferedReader(fr);
					while ((Keyword = read.readLine()) != null) {
						search(Keyword);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}

	}

	private void search(String searchTerm){
		
		Contents c = new Contents(searchTerm);
		Sparql s = new Sparql();
		s.getURI(c);
		s.getContents(c);
		
		FileCtrl f = new FileCtrl();
		f.saveResult(c);
		
		
		/*
		Iterator<ContentsItem> iter= c.getContent().iterator(); 
		ContentsItem tempItem;
		while(iter.hasNext())
		{
			tempItem=iter.next();
			System.out.println("#"+tempItem.getLink());
			System.out.println(tempItem.getContent());
		}*/
	}
}
