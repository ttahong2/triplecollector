package ex;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class Parser {
	/* Sample JSon structure of EndPoint
	 * 
	{ "head": { "link": [], "vars": ["URI"] },
		"results": { "distinct": false, "ordered": true, "bindings": [
    { "URI": { "type": "uri", "value": "http://dbpedia.org/resource/Hans_Lunding" }} ] } }
    
	*/
	public void JsonParse(String res) {
		JSONParser jsonParser = new JSONParser();
		JSONObject resultJson;
		try {
			resultJson = (JSONObject) jsonParser.parse(res);
			JSONObject response = (JSONObject) resultJson.get("results");
			JSONArray entities = (JSONArray) response.get("bindings");
			for (Object object : entities) {
				JSONObject obj = (JSONObject) object;
				JSONObject uri = (JSONObject) obj.get("URI");
				System.out.println(uri.get("value"));
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	public Contents JsonParse(String res, Contents c) {
		JSONParser jsonParser = new JSONParser();
		JSONObject resultJson;
		try {
			resultJson = (JSONObject) jsonParser.parse(res);
			JSONObject response = (JSONObject) resultJson.get("results");
			JSONArray entities = (JSONArray) response.get("bindings");
			ContentsItem temp;
			for (Object object : entities) {
				JSONObject obj = (JSONObject) object;
				JSONObject uri = (JSONObject) obj.get("URI");
				temp=  new ContentsItem((uri.get("value").toString()));
				c.getContent().add(temp);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return c;
	}
	public Contents parseURI(Contents c, String res){
		c = JsonParse(res, c);
		return c;
	}
}
